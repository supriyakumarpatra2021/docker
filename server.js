const ronin = require('ronin-server')
const mocks = require('ronin-mocks')

const server = ronin.server()

server.use('/', mocks.server(server.Router(), false, true))
//
// S T A R T    T H E    S E R V E R
//
server.start()